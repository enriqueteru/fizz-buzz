describe('Testing Fizz-Buzz', () => {


    const fizzBuzz = 15
    const buzz = 10
    const fizz = 6

    const FizzBuzz = (num)=> ((num % 3 === 0) && (num % 5 === 0) ? num = 'Fizz-Buzz' : Buzz(num));
    const Buzz = (num)=> ((num % 5 === 0 ) ? num = 'Buzz' : Fizz(num) );
    const Fizz = (num) => ((num % 3 === 0)  ? num = 'Fizz' : num);
    
    test('should 1 === 1', () => {
        expect(1).toEqual(1)
    });

   test(`should return 'Fizz-Buzz' if ${fizzBuzz} % 0`, () => {
    expect(FizzBuzz(fizzBuzz)).toBe('Fizz-Buzz')       
   });

   test(`should return 'Buzz' if ${buzz} % 0`, () => {
    expect(FizzBuzz(buzz)).toBe('Buzz')       
   });

   test(`should return 'Fizz' if ${fizz} % 0`, () => {
    expect(FizzBuzz(fizz)).toBe('Fizz')       
   });



});